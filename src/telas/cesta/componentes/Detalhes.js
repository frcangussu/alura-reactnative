import react from "react";
import Texto from "../../../componentes/Texto";
import { StyleSheet, View, Image } from "react-native";
import Botao from "../../../componentes/Botao";

export default function Detalhes({ logo, nome, fazenda, descricao, preco, botao }) {
    return <>
    <Texto style={estilos.nome}>{nome}</Texto>
    <View style={estilos.row}>
        <Image source={logo}  style={estilos.logo}/>
        <Texto style={estilos.fazenda}>{fazenda}</Texto>
    </View>
    <Texto style={estilos.descricao}>{descricao}</Texto>
    <Texto style={estilos.preco}>{preco}</Texto>
    <Botao>{botao}</Botao>
    </>
}

const estilos = StyleSheet.create({
    nome: {
        fontSize: 26,
        color: "#464646",
        lineHeight: 42,
        fontWeight: "bold"
    },
    logo: {
        width: 32,
        height: 32,
    },
    fazenda: {
        fontSize: 16,
        lineHeight: 26,
        marginLeft: 10
    },
    descricao: {
        color: "#A3A3A3",
        fontSize: 16,
        lineHeight: 26
    },
    preco: {
        color: "#2A9F85",
        fontWeight: "bold",
        fontSize: 26,
        lineHeight: 42,
        marginTop: 8
    },
    row: {
        flexDirection: "row",
        paddingVertical: 10,
    }
});